package com.example.movie.fintechapp.dagger

import android.app.Application
import android.content.Context
import dagger.Binds
import dagger.Module

@Module
abstract class AppModule{
    @Binds
    abstract fun bindContext(context: Application): Context
}