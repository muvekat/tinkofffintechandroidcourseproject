package com.example.movie.fintechapp.ui.test


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer

import com.example.movie.fintechapp.R
import com.example.movie.fintechapp.data.network.vo.ProblemTransformed
import com.example.movie.fintechapp.utils.WrapContentListView

class TestFragment : Fragment() {

    val mProblem = MutableLiveData<ProblemTransformed>()

    val position: Int
        get() = mProblem.value!!.position

    val answersLiveData = MutableLiveData<CharArray>()

    private val mProblemObserver = Observer<ProblemTransformed> { problem ->
        view?.findViewById<TextView>(R.id.question)?.text = problem.unstyledStatement
            .removeSurrounding("<p class=\"paragraph\">", "</p>")
        answersLiveData.value = (problem.lastSubmission ?: "0".repeat(problem.answerChoices.size)).toCharArray()

        view?.findViewById<WrapContentListView>(R.id.answersPlaceholder)?.adapter =
                ProblemAnswerArrayAdapter(context!!, problem, answersLiveData)
    }

    companion object {
        fun getInstance(problem: ProblemTransformed): TestFragment {
            val fragment = TestFragment()
            fragment.mProblem.value = problem
            return fragment
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_test, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        mProblem.observe(this, mProblemObserver)
    }


}
