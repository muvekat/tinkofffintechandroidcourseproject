package com.example.movie.fintechapp.data

import com.example.movie.fintechapp.data.local.db.UserDao
import com.example.movie.fintechapp.data.network.api.TinkoffService
import com.example.movie.fintechapp.data.network.vo.Homework
import com.example.movie.fintechapp.data.network.vo.TestAnswers
import com.example.movie.fintechapp.data.sharedents.User
import com.google.gson.Gson
import com.google.gson.JsonObject
import io.reactivex.*
import io.reactivex.schedulers.Schedulers
import java.lang.Exception
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DataRepository @Inject constructor(
    private val mUserDao: UserDao,
    private val mTinkoffService: TinkoffService,
    private val mGson: Gson
) {


    /**
     * Get a {@link Single}, which emits {@link User} from network.
     *
     * @return {@link Single}, which emits {@link User} on success, or error on error (including non-Ok status from request).
     */
    fun getUserRemote(): Single<User> {
        return mTinkoffService.getUserInfo()
            .flatMap {
                val status = it.get("status").asString
                if (status != "Ok") {
                    return@flatMap Single.error<User>(Exception("Received non-Ok status from getUserInfo"))
                }

                val user = mGson.fromJson<User>(it.get("user"), User::class.java)
                Single.just(user)
            }
            .subscribeOn(Schedulers.io())
    }

    /**
     * Get a {@link Single}, which emits {@link User} from local storage.
     *
     * @return {@link Single}, which emits {@link User} on success, or error on error(database is empty?).
     */
    fun getUserLocal() = mUserDao.getOneUser().subscribeOn(Schedulers.io())!!

    /**
     * Post a sign_in request and return a {@link Single}, which emits {@link User} and stores emitted {@link User}
     * to local storage.
     */
    fun signIn(email: String, password: String): Single<User> {
        return mTinkoffService.postSignIn(email, password)
            .flatMap {
                mUserDao.deleteAllUsers()
                    .andThen(mUserDao.insertUser(it))
                    .toSingle { it }
            }
            .subscribeOn(Schedulers.io())
    }

    /**
     * Get a {@link Single}, which emits list of {@link Homework}.
     */
    fun getHomeworks(): Single<List<Homework>> {
        return mTinkoffService.getHomewokrs()
            .map {
                it.homeworkList
            }
            .subscribeOn(Schedulers.io())
    }

    /**
     * Post a request to edit {@link User} info and update locally stored {@link User}.
     */
    fun editUser(user: User): Completable {

        return mTinkoffService.getUserInfo()
            .flatMapCompletable { sourceObject ->
                val status = sourceObject.get("status").asString
                if (status != "Ok") {
                    return@flatMapCompletable Completable.error(Exception("Received non-Ok status from getUserInfo"))
                }

                val userObject = getUserObject(sourceObject, user)

                mTinkoffService.postUserInfo(userObject)
                    .andThen(mUserDao.insertUser(user))
            }
            .subscribeOn(Schedulers.io())

    }

    private fun getUserObject(
        sourceObject: JsonObject,
        user: User
    ): JsonObject {
        return sourceObject.getAsJsonObject("user").apply {
            addProperty("birthday", user.birthday)
            addProperty("first_name", user.firstName)
            addProperty("middle_name", user.middleName)
            addProperty("last_name", user.lastName)
            addProperty("skype_login", user.skypeLogin)
            addProperty("description", user.description)
            addProperty("phone_mobile", user.phoneMobile)
            addProperty("region", user.region)
            addProperty("school", user.school)
            addProperty("school_graduation", user.schoolGraduation)
            addProperty("university", user.university)
            addProperty("university_graduation", user.universityGraduation)
            addProperty("avatar", user.avatar)
            addProperty("current_work", user.currentWork)
            addProperty("department", user.department)
            addProperty("faculty", user.faculty)
            addProperty("grade", user.grade)
            addProperty("resume", user.resume)
        }
    }

    /**
     * Post a request to start contest.
     *
     * @param link String to contest stored in {@link ContestInfo}.
     */
    fun startContest(link: String): Completable {
        return mTinkoffService.postStartContest(link).subscribeOn(Schedulers.io())
    }

    /**
     * Get {@link Status} of contest.
     *
     * @param link String to contest stored in {@link ContestInfo}.
     */
    fun getContestStatus(link: String) = mTinkoffService.getContestStatus(link).subscribeOn(Schedulers.io())!!

    /**
     * Get list of {@link ProblemWrapped} of requested contest.
     *
     * @param link String to contest stored in {@link ContestInfo}.
     */
    fun getContestProblems(link: String) = mTinkoffService.getContestProblems(link).subscribeOn(Schedulers.io())!!

    /**
     * Post answers for given question of current contest.
     *
     * @param link String to contest stored in {@link ContestInfo}.
     * @param position Number of question to which answers are given.
     * @param answers String, representing the answer(i.e. '100').
     */
    fun postAnswers(link: String, position: Int, answers: String): Completable {
        val wrappedAnswers = TestAnswers(answers)
        return mTinkoffService.postContest(link, position, wrappedAnswers).subscribeOn(Schedulers.io())
    }

    /**
     * Post a sign out request.
     */
    fun signOut() = mTinkoffService.postSignOut().subscribeOn(Schedulers.io())!!


}