package com.example.movie.fintechapp.ui.home.settings

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import com.example.movie.fintechapp.data.DataManager
import com.example.movie.fintechapp.ui.State
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class SettingsViewModel @Inject constructor(private val mDataManager: DataManager) {

    val exitButttonStateLiveData = MutableLiveData<State>()

    private val mCompositeDisposable = CompositeDisposable()

    fun signOut() {
        exitButttonStateLiveData.value = State.LOADING
        mCompositeDisposable.add(
            mDataManager.signOut()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    exitButttonStateLiveData.value = State.SUCCESS
                }, { e ->
                    exitButttonStateLiveData.value = State.ERROR
                    e.printStackTrace()
                })
        )
    }

    fun dispose(){
        mCompositeDisposable.clear()
    }
}