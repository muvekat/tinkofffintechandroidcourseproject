package com.example.movie.fintechapp.utils

import android.view.View
import com.google.android.material.snackbar.Snackbar

fun showSnackbar(v: View?, text: String?){
    if (v == null || text == null)
        return

    Snackbar.make(v, text, Snackbar.LENGTH_LONG).show()
}

fun showSnackbar(v: View?, stringId: Int?){
    if (v == null || stringId == null)
        return

    Snackbar.make(v, stringId, Snackbar.LENGTH_LONG).show()
}