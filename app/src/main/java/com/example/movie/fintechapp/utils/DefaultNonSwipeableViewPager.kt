package com.example.movie.fintechapp.utils

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.viewpager.widget.ViewPager

class DefaultNonSwipeableViewPager(context: Context, attrs: AttributeSet) : ViewPager(context, attrs) {

    private var enabled = false

    override fun onTouchEvent(event: MotionEvent): Boolean {
        return this.enabled && super.onTouchEvent(event) && performClick()
    }

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        return this.enabled && super.onInterceptTouchEvent(event)

    }

    override fun performClick(): Boolean {
        return this.enabled && super.performClick()
    }

    override fun setEnabled(enabled: Boolean) {
        this.enabled = enabled
    }

    override fun setCurrentItem(item: Int) {
        super.setCurrentItem(item, false)
    }
}
