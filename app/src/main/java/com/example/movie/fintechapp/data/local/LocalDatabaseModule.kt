package com.example.movie.fintechapp.data.local

import android.app.Application
import androidx.room.Room
import com.example.movie.fintechapp.data.local.db.UserDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class LocalDatabaseModule{
    @Provides
    @Singleton
    fun provideUserDatabase(context:Application): UserDatabase {
        return Room.databaseBuilder(context.applicationContext, UserDatabase::class.java, "User.db")
            .fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    @Singleton
    fun provideUserDao(db: UserDatabase) = db.getUserDao()
}