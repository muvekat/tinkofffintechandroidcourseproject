package com.example.movie.fintechapp.ui.splashscreen

import android.os.Bundle
import com.example.movie.fintechapp.R
import com.example.movie.fintechapp.data.DataManager
import com.example.movie.fintechapp.ui.home.HomeActivity
import com.example.movie.fintechapp.ui.login.LoginActivity
import com.example.movie.fintechapp.utils.startSingleActivityFromClass
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class LaunchScreen : DaggerAppCompatActivity() {

    @Inject
    lateinit var mDataManager: DataManager

    private var mDisposable: Disposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme_Launcher)
        super.onCreate(savedInstanceState)

        mDisposable = mDataManager.getUser()
            .subscribe({
                this.startSingleActivityFromClass(HomeActivity::class.java)
                finish()
            }, {
                this.startSingleActivityFromClass(LoginActivity::class.java)
                finish()
            })
    }

    override fun onStop() {
        super.onStop()
        mDisposable?.dispose()
    }
}
