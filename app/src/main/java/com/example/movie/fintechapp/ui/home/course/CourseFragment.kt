package com.example.movie.fintechapp.ui.home.course

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.asynclayoutinflater.view.AsyncLayoutInflater
import androidx.lifecycle.Observer
import com.example.movie.fintechapp.R
import com.example.movie.fintechapp.dagger.scopes.ActivityScope
import com.example.movie.fintechapp.data.network.vo.SerializedStatus
import com.example.movie.fintechapp.data.network.vo.TaskTransformed
import com.example.movie.fintechapp.databinding.FragmentCourseBinding
import com.example.movie.fintechapp.databinding.FragmentPlaceholderBinding
import com.example.movie.fintechapp.ui.State
import com.example.movie.fintechapp.ui.test.TestActivity
import com.example.movie.fintechapp.utils.showSnackbar
import com.example.movie.fintechapp.utils.startActivityFromClass
import dagger.android.support.DaggerFragment
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@ActivityScope
class CourseFragment @Inject constructor() : DaggerFragment() {

    @Inject
    lateinit var mCourseViewModel: CourseViewModel

    @Inject
    lateinit var mTestAdapter: TestAdapter

    private var mDisposable: Disposable? = null


    private val mStateObserver = Observer<State> { state ->
        when (state!!) {
            State.SUCCESS -> hideLoading()
            State.ERROR -> {
                hideLoading(); showError()
            }
            State.LOADING -> showLoading()
        }
    }


    private val mDataObserver = Observer<List<TaskTransformed>> {
        setAdapterData(it)
    }


    private val mStartTestActivityObserver = Observer<Unit> {
        context!!.startActivityFromClass(TestActivity::class.java)
    }


    private lateinit var mCourseBinding: FragmentCourseBinding
    private lateinit var mPlaceholderBinding: FragmentPlaceholderBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mPlaceholderBinding = FragmentPlaceholderBinding.inflate(inflater, container, false)
        return mPlaceholderBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //Use AsyncLayoutInflater since inflating this layout takes long
        AsyncLayoutInflater(context!!).inflate(
            R.layout.fragment_course,
            mPlaceholderBinding.fragmentLayoutPlaceholder as ViewGroup
        ) { inflatedView, _, container ->
            //Continue initialization if parent isn't null by the time inflater finished.
            if (container != null) {
                initializeFragment(container, inflatedView)
            }
        }
    }

    override fun onStop() {
        super.onStop()
        mDisposable?.dispose()
    }

    private fun initializeFragment(container: ViewGroup, view: View) {
        mPlaceholderBinding.progressBar.visibility = View.GONE

        container.addView(view)

        mCourseBinding = FragmentCourseBinding.bind(view)

        mCourseBinding.testRecyclerView.adapter = mTestAdapter
        mCourseBinding.swipeToRefresh.setOnRefreshListener { mCourseViewModel.loadTasksRemote() }

        mCourseViewModel.stateLiveData.observe(this, mStateObserver)
        mCourseViewModel.taskLiveData.observe(this, mDataObserver)
        mCourseViewModel.startTestActivityLiveEvent.observe(this, mStartTestActivityObserver)
    }

    private fun setAdapterData(list: List<TaskTransformed>) {
        mTestAdapter.setData(list)

        val index = list.indexOfFirst { it.contestStatus == SerializedStatus.ONGOING }
        if (index != -1) {
            //Wait until all updates are executed because smooth scrolling may be executed before updating RecyclerView.
            mDisposable = Observable.interval(100, TimeUnit.MILLISECONDS)
                .skipWhile { mCourseBinding.testRecyclerView.hasPendingAdapterUpdates() }
                .first(1)
                .subscribe { _ -> mCourseBinding.testRecyclerView.smoothScrollToPosition(index) }
        }
    }

    private fun showLoading() {
        mCourseBinding.swipeToRefresh.isRefreshing = true
    }

    private fun hideLoading() {
        mCourseBinding.swipeToRefresh.isRefreshing = false
    }

    private fun showError() {
        showSnackbar(mCourseBinding.swipeToRefresh, R.string.error_occured)
    }

}
