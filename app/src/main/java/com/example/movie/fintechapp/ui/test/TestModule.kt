package com.example.movie.fintechapp.ui.test

import com.example.movie.fintechapp.dagger.scopes.ActivityScope
import com.example.movie.fintechapp.data.DataManager
import dagger.Module
import dagger.Provides

@Module
class TestModule {
    @Provides
    @ActivityScope
    fun provideTestViewModel(dataManager: DataManager) = TestViewModel(dataManager)
}