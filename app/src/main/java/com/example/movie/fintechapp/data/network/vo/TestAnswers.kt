package com.example.movie.fintechapp.data.network.vo

import androidx.room.Entity
import com.google.gson.annotations.SerializedName

@Entity
data class TestAnswers(
    @SerializedName("answer") val answer:String,
    @SerializedName("language") val language:Int = 1,
    @SerializedName("file") val file:String? = null
)