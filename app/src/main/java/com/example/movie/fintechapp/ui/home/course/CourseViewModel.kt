package com.example.movie.fintechapp.ui.home.course

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import com.example.movie.fintechapp.data.DataManager
import com.example.movie.fintechapp.data.network.vo.Task
import com.example.movie.fintechapp.data.network.vo.TaskTransformed
import com.example.movie.fintechapp.ui.State
import com.example.movie.fintechapp.utils.SingleLiveEvent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.BiConsumer
import javax.inject.Inject

class CourseViewModel @Inject constructor(private val mDataManager: DataManager) {

    val stateLiveData = MutableLiveData<State>()
    val taskLiveData = MutableLiveData<List<TaskTransformed>>()
    val startTestActivityLiveEvent = SingleLiveEvent<Unit>()

    private val mCompositeDisposable = CompositeDisposable()

    private val mTaskListConsumer =
        BiConsumer<List<TaskTransformed>, Throwable> { t1, t2 ->
            if (t1 != null) {
                stateLiveData.postValue(State.SUCCESS)
                taskLiveData.postValue(t1)
            } else if (t2 != null) {
                stateLiveData.postValue(State.ERROR)
                t2.printStackTrace()
            }
        }


    init {
        loadTasks()
    }

    fun loadTasks() {
        stateLiveData.postValue(State.LOADING)
        mCompositeDisposable.add(
            mDataManager.getTaskList()
                .subscribe(mTaskListConsumer)
        )
    }

    fun loadTasksRemote() {
        stateLiveData.postValue(State.LOADING)
        mCompositeDisposable.add(
            mDataManager.getTaskListRemote()
                .subscribe(mTaskListConsumer)
        )
    }

    fun startContest(link: String) {
        stateLiveData.postValue(State.LOADING)
        mCompositeDisposable.add(
            mDataManager.startContest(link)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    stateLiveData.postValue(State.SUCCESS)
                    startTestActivityLiveEvent.call()
                }, {
                    stateLiveData.postValue(State.ERROR)
                })
        )
    }

    fun dispose(){
        mCompositeDisposable.clear()
    }
}
