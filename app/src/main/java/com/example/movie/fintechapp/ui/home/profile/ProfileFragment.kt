package com.example.movie.fintechapp.ui.home.profile


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.asynclayoutinflater.view.AsyncLayoutInflater
import androidx.lifecycle.Observer

import com.example.movie.fintechapp.R
import com.example.movie.fintechapp.dagger.scopes.ActivityScope
import com.example.movie.fintechapp.databinding.FragmentPlaceholderBinding
import com.example.movie.fintechapp.databinding.FragmentProfileBinding
import com.example.movie.fintechapp.ui.State
import com.example.movie.fintechapp.utils.showSnackbar
import dagger.android.support.DaggerFragment
import javax.inject.Inject

@ActivityScope
class ProfileFragment @Inject constructor() : DaggerFragment() {

    @Inject
    lateinit var mProfileViewModel: ProfileViewModel


    private val mStateObserver = Observer<State> { state ->
        when (state!!) {
            State.SUCCESS -> hideLoading()
            State.ERROR -> {
                hideLoading()
                showError()
            }
            State.LOADING -> showLoading()
        }
    }


    private val mPendingBindingsObserver = Observer<Unit> { mProfileBinding.executePendingBindings() }

    private lateinit var mProfileBinding: FragmentProfileBinding
    private lateinit var mPlaceholderBinding: FragmentPlaceholderBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mPlaceholderBinding = FragmentPlaceholderBinding.inflate(inflater, container, false)
        return mPlaceholderBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //Use AsyncLayoutInflater since inflating this layout takes long
        AsyncLayoutInflater(context!!).inflate(
            R.layout.fragment_profile,
            mPlaceholderBinding.fragmentLayoutPlaceholder as ViewGroup
        )
        { inflatedView, _, container ->
            //Continue initialization if parent isn't null by the time inflater finished.
            if (container != null) {
                initializeFragment(container, inflatedView)
            }
        }

    }

    private fun initializeFragment(container: ViewGroup?, view: View) {
        mPlaceholderBinding.progressBar.visibility = View.GONE

        container!!.addView(view)

        mProfileBinding = FragmentProfileBinding.bind(view)

        mProfileBinding.viewmodel = mProfileViewModel
        mProfileBinding.swipeToRefresh.setOnRefreshListener { mProfileViewModel.loadDataRemote() }

        mProfileViewModel.stateLiveData.observe(this, mStateObserver)
        mProfileViewModel.executePendingEvent.observe(this, mPendingBindingsObserver)
    }

    private fun showLoading() {
        mProfileBinding.swipeToRefresh.isRefreshing = true
    }

    private fun hideLoading() {
        mProfileBinding.swipeToRefresh.isRefreshing = false
    }

    private fun showError() {
        showSnackbar(mProfileBinding.swipeToRefresh, R.string.error_occured)
    }

}
