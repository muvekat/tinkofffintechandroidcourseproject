package com.example.movie.fintechapp.utils

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.ListAdapter
import android.widget.ListView

class WrapContentListView : ListView {
    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle)

    public override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val expandSpec = View.MeasureSpec.makeMeasureSpec(
            Integer.MAX_VALUE shr 2,
            View.MeasureSpec.AT_MOST
        )
        super.onMeasure(widthMeasureSpec, expandSpec)
    }

    override fun setAdapter(adapter: ListAdapter) {
        super.setAdapter(adapter)
        setHeightWrapContent()
    }

    fun setHeightWrapContent() {
        val listAdapter = adapter ?: return
        var totalHeight = 0
        for (i in 0 until listAdapter.count) {
            val listItem = listAdapter.getView(i, null, this)
            listItem.measure(0, 0)
            totalHeight += listItem.measuredHeight
        }

        val params = this.layoutParams

        params.height = totalHeight + this.dividerHeight * (listAdapter.count - 1)
        this.layoutParams = params
    }
}
