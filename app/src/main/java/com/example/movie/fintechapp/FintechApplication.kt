package com.example.movie.fintechapp

import com.example.movie.fintechapp.dagger.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

class FintechApplication: DaggerApplication(){

    override fun applicationInjector(): AndroidInjector<out DaggerApplication>
        = DaggerAppComponent.builder().application(this).build()

}