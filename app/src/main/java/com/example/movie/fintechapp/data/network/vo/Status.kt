package com.example.movie.fintechapp.data.network.vo

import androidx.room.Entity
import com.google.gson.annotations.SerializedName

@Entity
data class Status(
    @SerializedName("contest") val contest:Contest,
    @SerializedName("problems") val problemList:List<String?>
)

@Entity
data class Contest(
    @SerializedName("duration") val duration:Int,
    @SerializedName("time_left") val timeLeft:Int,
    @SerializedName("title") val title:String
)

class ContestTransformed(
    val title: String,
    val duration:Int,
    val timeLeft: Int,
    val problemList: List<ProblemTransformed>
)