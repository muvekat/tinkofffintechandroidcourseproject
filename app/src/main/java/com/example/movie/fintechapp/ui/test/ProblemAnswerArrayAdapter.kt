package com.example.movie.fintechapp.ui.test

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.lifecycle.MutableLiveData
import com.example.movie.fintechapp.R
import com.example.movie.fintechapp.data.network.vo.ProblemTransformed

class ProblemAnswerArrayAdapter(
    context: Context,
    private val mProblem: ProblemTransformed,
    private val mAnswersLiveData: MutableLiveData<CharArray>
) :
    ArrayAdapter<String>(context, R.layout.answer_item, mProblem.answerChoices) {

    private val isRadioButton = mProblem.problemType == "SELECT_ONE"
    private var buttonToDeselect: CompoundButton? = null

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val mView = convertView ?: LayoutInflater.from(context).inflate(R.layout.answer_item, parent, false)

        mView.findViewById<TextView>(R.id.answerTextView).text = getItem(position)

        val button: CompoundButton
        button = if (isRadioButton) {
            mView.findViewById<RadioButton>(R.id.radioButton)
        } else {
            mView.findViewById<CheckBox>(R.id.checkbox)
        }
        button.visibility = View.VISIBLE

        val checked = mAnswersLiveData.value!![position] == '1'
        button.isChecked = checked
        if (checked && isRadioButton) buttonToDeselect = button

        mView.setOnClickListener {
            val answersString: CharArray
            if (isRadioButton) {
                answersString = "0".repeat(mProblem.answerChoices.size).toCharArray()

                buttonToDeselect?.isChecked = false

                answersString[position] = '1'
                button.isChecked = true

                buttonToDeselect = button
            } else {
                answersString = mAnswersLiveData.value!!

                val wasChecked = answersString[position] == '1'
                if (wasChecked) {
                    answersString[position] = '0'
                    button.isChecked = false
                } else {
                    answersString[position] = '1'
                    button.isChecked = true
                }
            }

            mAnswersLiveData.postValue(answersString)
        }

        return mView
    }
}