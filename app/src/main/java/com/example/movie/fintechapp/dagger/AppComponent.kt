package com.example.movie.fintechapp.dagger

import android.app.Application
import com.example.movie.fintechapp.FintechApplication
import com.example.movie.fintechapp.data.DataModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [ActivityBindingModule::class,
        AppModule::class,
        DataModule::class,
        AndroidSupportInjectionModule::class]
)
interface AppComponent: AndroidInjector<FintechApplication> {

    @Component.Builder
    interface Builder{
        @BindsInstance
        fun application(application: Application): AppComponent.Builder

        fun build(): AppComponent
    }

}