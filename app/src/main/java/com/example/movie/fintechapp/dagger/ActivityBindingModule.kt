package com.example.movie.fintechapp.dagger

import com.example.movie.fintechapp.dagger.scopes.ActivityScope
import com.example.movie.fintechapp.ui.home.HomeActivity
import com.example.movie.fintechapp.ui.home.HomeModule
import com.example.movie.fintechapp.ui.login.LoginActivity
import com.example.movie.fintechapp.ui.login.LoginModule
import com.example.movie.fintechapp.ui.splashscreen.LaunchScreen
import com.example.movie.fintechapp.ui.test.TestActivity
import com.example.movie.fintechapp.ui.test.TestModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {
    @ActivityScope
    @ContributesAndroidInjector(modules = [LoginModule::class])
    abstract fun loginActivity(): LoginActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [HomeModule::class])
    abstract fun homeActivity(): HomeActivity

    @ActivityScope
    @ContributesAndroidInjector(modules = [TestModule::class])
    abstract fun testActivity(): TestActivity

    @ActivityScope
    @ContributesAndroidInjector
    abstract fun splashScreen(): LaunchScreen
}