package com.example.movie.fintechapp.ui

enum class State {
    SUCCESS,
    ERROR,
    LOADING
}