package com.example.movie.fintechapp.ui.test

import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.example.movie.fintechapp.R
import com.example.movie.fintechapp.data.DataManager
import com.example.movie.fintechapp.data.network.vo.ContestTransformed
import com.example.movie.fintechapp.data.network.vo.ProblemTransformed
import com.example.movie.fintechapp.ui.State
import com.example.movie.fintechapp.utils.showSnackbar
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_test.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class TestActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var mDataManager: DataManager

    @Inject
    lateinit var mTestViewModel: TestViewModel

    private val mFragmentList = mutableListOf<TestFragment>()

    private val mProblemList = mutableListOf<ProblemTransformed>()

    private val mCurrentFragmentLiveData = MutableLiveData<Int>()

    private val mProblemSelectorAdapter = ProblemSelectorAdapter(mCurrentFragmentLiveData)

    private var mCountDownTimer: CountDownTimer? = null

    private val mIsSignedOffObserver = Observer<Boolean> { if (it) finish() }

    private val mStateObserver = Observer<State> { state ->
        when (state!!) {
            State.SUCCESS -> hideLoading()
            State.ERROR -> {
                hideLoading(); showError()
            }
            State.LOADING -> showLoading()
        }
    }

    private val mOpenNextObserver = Observer<Unit> {
        val currentFragmentIndex = mCurrentFragmentLiveData.value
        if (currentFragmentIndex != null)
            mCurrentFragmentLiveData.value = currentFragmentIndex + 1
        else
            finish()
    }

    private val mRightButtonListener = View.OnClickListener {
        val currentFragmentIndex = mCurrentFragmentLiveData.value
        if (currentFragmentIndex != null) {
            submitAnswers(currentFragmentIndex)
        } else
            finish()
    }

    private val mContestObserver = Observer<ContestTransformed> { contest ->
        testTitle.text = contest.title
        startCountdownTimer(contest)
    }

    private val mProblemListObserver = Observer<List<ProblemTransformed>> {inputList ->
        showLoading()
        initializeProblemListAndFragments(inputList)
        hideLoading()
    }

    init {
        mCurrentFragmentLiveData.observe(this, Observer {position ->
            if (position in mProblemList.indices){
                openFragment(position)
                updateRightButtonText(position)
            } else{
                finish()
            }
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)

        initializeActivity()

    }

    private fun initializeActivity() {
        setSupportActionBar(testToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowCustomEnabled(true)
        supportActionBar?.title = ""

        testSelectorListView.adapter = mProblemSelectorAdapter

        mDataManager.isSignedOff.observe(this, mIsSignedOffObserver)

        mTestViewModel.stateLiveData.observe(this, mStateObserver)
        mTestViewModel.contestLiveData.observe(this, mContestObserver)
        mTestViewModel.problemListLiveData.observe(this, mProblemListObserver)
        mTestViewModel.openNextLiveEvent.observe(this, mOpenNextObserver)

        leftButton.setOnClickListener { mTestViewModel.openNext() }
        rightButton.setOnClickListener(mRightButtonListener)
    }

    override fun onStop() {
        super.onStop()
        mCountDownTimer?.cancel()
        mTestViewModel.dispose()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onBackPressed() {
        val currentFragmentIndex = mCurrentFragmentLiveData.value
        if (currentFragmentIndex != null)
            mCurrentFragmentLiveData.value = currentFragmentIndex - 1
        else
            finish()
    }

    private fun updateRightButtonText(position: Int) {
        if(position == mFragmentList.size-1) {
            rightButton.text = getText(R.string.end)
        } else {
            rightButton.text = getText(R.string.next)
        }
    }

    private fun showLoading() {
        loadingLayout.visibility = View.VISIBLE
    }

    private fun hideLoading() {
        loadingLayout.visibility = View.GONE
    }

    private fun showError() {
        showSnackbar(testConstraintLayout, R.string.error_occured)
    }

    private fun openFragment(position: Int) {
        questionNumber.text = getString(R.string.question).format(position+1,mFragmentList.size)
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragmentHolder, mFragmentList[position])
            .commitNow()
    }


    private fun startCountdownTimer(contest: ContestTransformed) {
        val millisTotal = TimeUnit.SECONDS.toMillis(contest.duration.toLong())
        val millisLeft = TimeUnit.SECONDS.toMillis(contest.timeLeft.toLong())
        val millisStep = TimeUnit.SECONDS.toMillis(1)

        val step = (1000.0 / (millisTotal / millisStep))
        val initialProgress = ((millisTotal - millisLeft) * step).toInt()

        timeLeftProgressBar.progress = initialProgress

        mCountDownTimer?.cancel()
        mCountDownTimer = object : CountDownTimer(millisLeft, millisStep) {
            var i: Long = (millisTotal - millisLeft) / millisStep

            override fun onTick(millisUntilFinished: Long) {
                i++
                timeLeftProgressBar.progress = (i * step).toInt()
            }

            override fun onFinish() {
                timeLeftProgressBar.progress = 100
                finish()
            }
        }.start()
    }

    private fun initializeProblemListAndFragments(inputList: List<ProblemTransformed>) {
        mProblemSelectorAdapter.setData(inputList)

        mProblemList.clear()
        mProblemList.addAll(inputList)

        for (index in mProblemList.indices) {
            val problem = mProblemList[index]
            var fragment = mFragmentList.getOrNull(index)
            if (fragment != null) {
                if (fragment.mProblem.value != problem)
                    fragment.mProblem.postValue(problem)
            } else {
                fragment = TestFragment.getInstance(problem)
                mFragmentList.add(fragment)
            }
        }

        if (mCurrentFragmentLiveData.value == null)
            mCurrentFragmentLiveData.value = 0
    }

    private fun submitAnswers(currentFragmentIndex: Int) {
        val currentFragment = mFragmentList[currentFragmentIndex]
        val position = mProblemList[currentFragmentIndex].position
        val answers = currentFragment.answersLiveData.value?.joinToString("")
        if (answers != null)
            mTestViewModel.submitAnswersAndOpenNext(answers, currentFragmentIndex, position)
        //Else - do nothing since this should occur only when fragment has not been initialized yet.
    }

    fun doNothing(v: View) {} //Function which is used to intercept click events on loading layout.

}
