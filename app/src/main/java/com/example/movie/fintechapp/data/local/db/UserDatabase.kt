package com.example.movie.fintechapp.data.local.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.movie.fintechapp.data.sharedents.User

@Database(entities = [User::class], version = 2, exportSchema = false)
abstract class UserDatabase : RoomDatabase() {
    abstract fun getUserDao(): UserDao
}