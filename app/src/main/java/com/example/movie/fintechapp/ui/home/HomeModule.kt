package com.example.movie.fintechapp.ui.home

import com.example.movie.fintechapp.dagger.scopes.ActivityScope
import com.example.movie.fintechapp.dagger.scopes.FragmentScope
import com.example.movie.fintechapp.data.DataManager
import com.example.movie.fintechapp.ui.home.course.CourseFragment
import com.example.movie.fintechapp.ui.home.course.CourseViewModel
import com.example.movie.fintechapp.ui.home.profile.ProfileFragment
import com.example.movie.fintechapp.ui.home.profile.ProfileViewModel
import com.example.movie.fintechapp.ui.home.settings.SettingsFragment
import com.example.movie.fintechapp.ui.home.settings.SettingsViewModel
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector

@Module
abstract class HomeModule {

    /**
     * This companion object makes ViewModels singleton per activity (initializing them on fragment creation
     * makes changing tabs slow because of data binding and fetching).
     */
    @Module
    companion object{
        @JvmStatic
        @Provides
        @ActivityScope
        fun provideProfileViewModel(dataManager: DataManager) = ProfileViewModel(dataManager)

        @JvmStatic
        @Provides
        @ActivityScope
        fun provideCourseViewModel(dataManager: DataManager) = CourseViewModel(dataManager)

        @JvmStatic
        @Provides
        @ActivityScope
        fun provideSettingsViewModel(dataManager: DataManager) = SettingsViewModel(dataManager)
    }

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun provideCourseFragment(): CourseFragment

    @FragmentScope
    @ContributesAndroidInjector()
    abstract fun provideProfileFragment(): ProfileFragment

    @FragmentScope
    @ContributesAndroidInjector()
    abstract fun provideSettingsFragment(): SettingsFragment
}