package com.example.movie.fintechapp.data.network

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton
import android.app.Application
import com.example.movie.fintechapp.data.network.api.TinkoffService
import com.franmontiel.persistentcookiejar.PersistentCookieJar
import com.franmontiel.persistentcookiejar.cache.SetCookieCache
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.*

private const val URL = "https://fintech.tinkoff.ru/"

@Module
class NetworkServiceModule{

    @Provides
    @Singleton
    fun provideGson() =  GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create()

    @Provides
    @Singleton
    fun provideSetCookieCache() = SetCookieCache()

    @Provides
    @Singleton
    fun provideSharedPrefsCookiePersistor(context:Application) = SharedPrefsCookiePersistor(context.applicationContext)

    @Provides
    @Singleton
    fun provideCookieJar(cache: SetCookieCache, persistor: SharedPrefsCookiePersistor):CookieJar
            = PersistentCookieJar(cache, persistor)

    /**
     * Provides interceptor which adds X-CSRFToken and Referer header stored in CookieJar.
     */
    @Provides
    @Singleton
    fun provideInterceptor(jar: CookieJar): Interceptor {
        return Interceptor { chain ->
            val request = chain.request()
            val builder = request.newBuilder()

            val cookieList = jar.loadForRequest(request.url())
            val csrfCookie = cookieList.find { it.name() == "csrftoken" }
            if (csrfCookie != null) {
                val token = csrfCookie.value()
                builder.addHeader("X-CSRFToken", token)
                builder.addHeader("Referer", URL)
            }

            chain.proceed(builder.build())
        }
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(cookieJar: CookieJar, interceptor: Interceptor):OkHttpClient {
        return OkHttpClient.Builder().cookieJar(cookieJar).addInterceptor(interceptor).build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient, gson: Gson):Retrofit{
        return Retrofit.Builder()
            .baseUrl(URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()
    }

    @Provides
    @Singleton
    fun provideTinkoffService(retrofit: Retrofit) = retrofit.create(TinkoffService::class.java)!!

}