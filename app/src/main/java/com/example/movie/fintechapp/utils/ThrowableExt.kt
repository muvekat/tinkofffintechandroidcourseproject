package com.example.movie.fintechapp.utils


fun Throwable.isForbidden(): Boolean {
    if (this.message?.contains("forbidden", true) == true)
        return true
    if (this.message?.contains("Received non-Ok status", true) == true)
        return true
    return false
}