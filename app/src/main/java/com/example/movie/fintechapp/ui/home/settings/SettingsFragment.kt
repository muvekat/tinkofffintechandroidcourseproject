package com.example.movie.fintechapp.ui.home.settings


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer

import com.example.movie.fintechapp.R
import com.example.movie.fintechapp.ui.State
import com.example.movie.fintechapp.utils.showSnackbar
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_settings.*
import javax.inject.Inject

class SettingsFragment @Inject constructor() : DaggerFragment() {

    @Inject
    lateinit var mViewModel: SettingsViewModel

    private val mExitButtonStateObserver = Observer<State> { state ->
        when (state!!) {
            State.SUCCESS -> hideLoadingExitButton()
            State.ERROR -> {
                hideLoadingExitButton()
                showError()
            }
            State.LOADING -> showLoadingExitButton()
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mViewModel.exitButttonStateLiveData.observe(this, mExitButtonStateObserver)
        exitButton.setOnClickListener {
            mViewModel.signOut()
        }
    }

    private fun showLoadingExitButton() {
        exitButton.text = ""
        exitButton.isClickable = false
        exitProgressBar.visibility = View.VISIBLE
    }

    private fun hideLoadingExitButton() {
        exitButton.text = getText(R.string.exit)
        exitButton.isClickable = true
        exitProgressBar.visibility = View.GONE
    }

    private fun showError() {
        showSnackbar(exitLayout, R.string.error_occured)
    }
}
