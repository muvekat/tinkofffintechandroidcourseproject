package com.example.movie.fintechapp.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.core.content.ContextCompat.startActivity

fun Context.startSingleActivityFromClass(clazz: Class<out Activity>) {
    val intent = Intent(this, clazz)
    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
    startActivity(this, intent, null)
}

fun Context.startActivityFromClass(clazz: Class<out Activity>) {
    val intent = Intent(this, clazz)
    startActivity(this, intent, null)
}