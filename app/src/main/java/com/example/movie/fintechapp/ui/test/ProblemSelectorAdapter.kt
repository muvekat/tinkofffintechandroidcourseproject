package com.example.movie.fintechapp.ui.test

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.BaseAdapter
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.example.movie.fintechapp.R
import com.example.movie.fintechapp.data.network.vo.Problem
import com.example.movie.fintechapp.data.network.vo.ProblemTransformed
import com.example.movie.fintechapp.data.network.vo.SerializedStatus
import com.example.movie.fintechapp.data.network.vo.TaskTransformed
import com.example.movie.fintechapp.ui.home.course.CourseViewModel
import javax.inject.Inject

class ProblemSelectorViewHolder constructor(
    itemView: View,
    private val mCurrentFragmentLiveData: MutableLiveData<Int>,
    private val context: Context
) : RecyclerView.ViewHolder(itemView) {

    @BindView(R.id.testProblemText)
    lateinit var testTextView: TextView
    @BindView(R.id.testProblemCircle)
    lateinit var circleView: View

    init {
        ButterKnife.bind(this, itemView)
    }

    fun bind(problem: ProblemTransformed, ind: Int) {
        testTextView.text = problem.position.toString()

        if (problem.status == "AC") {
            circleView.background = context.getDrawable(R.drawable.test_problem_selector_item_ac_with_ripple)
        } else {
            circleView.background = context.getDrawable(R.drawable.test_problem_selector_item_null_with_ripple)
        }

        itemView.setOnClickListener {
            mCurrentFragmentLiveData.postValue(ind)
        }
    }
}

class ProblemSelectorAdapter(
    private val mCurrentFragmentLiveData: MutableLiveData<Int>
) : RecyclerView.Adapter<ProblemSelectorViewHolder>() {


    private val listOfProblems = mutableListOf<ProblemTransformed>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProblemSelectorViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.test_problem_selector_item, parent, false)
        return ProblemSelectorViewHolder(view, mCurrentFragmentLiveData, parent.context)
    }

    override fun getItemCount() = listOfProblems.size


    override fun onBindViewHolder(holder: ProblemSelectorViewHolder, position: Int) {
        holder.bind(listOfProblems[position], position)
    }

    fun setData(list: List<ProblemTransformed>) {
        listOfProblems.clear()
        listOfProblems.addAll(list)
        notifyDataSetChanged()
    }
}