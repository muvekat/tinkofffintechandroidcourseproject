package com.example.movie.fintechapp.ui.login

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import com.example.movie.fintechapp.dagger.scopes.ActivityScope
import com.example.movie.fintechapp.data.DataManager
import com.example.movie.fintechapp.ui.State
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

@ActivityScope
class LoginViewModel @Inject constructor(private val mDataManager: DataManager) {

    val loginState = MutableLiveData<State>()

    private val mCompositeDisposable = CompositeDisposable()

    fun logIn(email: String, password: String) {
        loginState.postValue(State.LOADING)
        mCompositeDisposable.add(
            mDataManager.signIn(email, password)
                .subscribe({
                    loginState.postValue(State.SUCCESS)
                }, {
                    loginState.postValue(State.ERROR)
                    it.printStackTrace()
                })
        )
    }

    fun dispose(){
        mCompositeDisposable.clear()
    }
}