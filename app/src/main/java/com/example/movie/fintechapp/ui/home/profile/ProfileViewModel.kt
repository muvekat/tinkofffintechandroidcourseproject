package com.example.movie.fintechapp.ui.home.profile

import android.annotation.SuppressLint
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.Observable
import androidx.databinding.ObservableBoolean
import androidx.databinding.library.baseAdapters.BR
import androidx.lifecycle.MutableLiveData
import com.example.movie.fintechapp.data.DataManager
import com.example.movie.fintechapp.data.sharedents.User
import com.example.movie.fintechapp.ui.State
import com.example.movie.fintechapp.utils.SingleLiveEvent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.BiConsumer
import javax.inject.Inject

class ProfileViewModel @Inject constructor(private val mDataManager: DataManager) : BaseObservable() {

    val stateLiveData = MutableLiveData<State>()
    val executePendingEvent = SingleLiveEvent<Unit>() //LiveEvent used to instruct fragment to execute bindings so that
    //we can be sure all EditTexts got updated data before attaching callback.
    private val mCompositeDisposable = CompositeDisposable()

    @get:Bindable
    var observableUser = ObservableUser(User())
        set(value) {
            field.removeOnPropertyChangedCallback(mOnObservableUserChangedCallback)

            field = value
            notifyPropertyChanged(BR.observableUser)
            executePendingEvent.call()

            field.addOnPropertyChangedCallback(mOnObservableUserChangedCallback)
            wasEdited.set(false)
        }

    //Callback which is used to make buttons clickable.
    private val mOnObservableUserChangedCallback = object : Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
            wasEdited.set(true)
        }
    }

    private val mUserConsumer =
        BiConsumer<User, Throwable> { t1, t2 ->
            if (t1 != null) {
                stateLiveData.value = State.SUCCESS
                observableUser = ObservableUser(t1)
            } else if (t2 != null) {
                stateLiveData.value = State.ERROR
                t2.printStackTrace()
            }
        }

    val wasEdited = ObservableBoolean()


    init {
        loadData()
    }


    fun loadData() {
        stateLiveData.value = State.LOADING

        mCompositeDisposable.add(
            mDataManager.getUser()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mUserConsumer)
        )
    }

    fun loadDataRemote() {
        stateLiveData.value = State.LOADING

        mCompositeDisposable.add(
            mDataManager.getUserRemote()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mUserConsumer)
        )
    }

    fun sendData() {
        stateLiveData.value = State.LOADING

        mCompositeDisposable.add(
            mDataManager.editUser(observableUser.getResultingUser())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    stateLiveData.value = State.SUCCESS
                    wasEdited.set(false)
                }, {
                    stateLiveData.value = State.ERROR
                    it.printStackTrace()
                })
        )
    }

    fun loadCachedUser() {
        stateLiveData.value = State.LOADING

        mCompositeDisposable.add(
            mDataManager.getUserCached()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mUserConsumer)
        )
    }

    fun dispose(){
        mCompositeDisposable.clear()
    }
}