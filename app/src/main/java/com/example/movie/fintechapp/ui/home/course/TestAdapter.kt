package com.example.movie.fintechapp.ui.home.course

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import com.example.movie.fintechapp.R
import com.example.movie.fintechapp.data.network.vo.SerializedStatus
import com.example.movie.fintechapp.data.network.vo.TaskTransformed
import javax.inject.Inject

class TestViewHolder constructor(itemView: View, private val mCourseViewModel:CourseViewModel)  : RecyclerView.ViewHolder(itemView) {

    @BindView(R.id.TaskShortName)
    lateinit var taskShortNameTextView: TextView
    @BindView(R.id.IsOngongIcon)
    lateinit var isOngoingIcon: View
    @BindView(R.id.taskResult)
    lateinit var taskResultTextView: TextView

    init {
        ButterKnife.bind(this, itemView)
    }

    fun bind(test: TaskTransformed) {
        taskShortNameTextView.text = test.shortName
        isOngoingIcon.visibility = if (test.contestStatus == SerializedStatus.ONGOING) {
            View.VISIBLE
        } else {
            View.GONE
        }

        if (test.contestStatus == SerializedStatus.ONGOING) {
            itemView.isClickable = true
            itemView.setOnClickListener {
                mCourseViewModel.startContest(test.contestUrl!!)
            }
        } else {
            //Set clickable to false so that we can be sure that recycled view is not clickable.
            itemView.isClickable = false
        }
        if (test.taskStatus == "accepted" || test.taskStatus == "failed"){
            taskResultTextView.visibility = View.VISIBLE
            taskResultTextView.text = test.mark
        } else {
            taskResultTextView.visibility = View.GONE
        }
    }
}

class TestAdapter @Inject constructor(private val mCourseViewModel:CourseViewModel): RecyclerView.Adapter<TestViewHolder>(){

    private val listOfTasks = mutableListOf<TaskTransformed>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TestViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.task_list_item, parent, false)
        return TestViewHolder(view, mCourseViewModel)
    }

    override fun getItemCount() = listOfTasks.size


    override fun onBindViewHolder(holder: TestViewHolder, position: Int) {
        holder.bind(listOfTasks[position])
    }

    fun setData(list: List<TaskTransformed>){
        listOfTasks.clear()
        listOfTasks.addAll(list)
        notifyDataSetChanged()
    }
}