package com.example.movie.fintechapp.data.local.db;

import androidx.room.*;
import com.example.movie.fintechapp.data.sharedents.User;
import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.functions.Action;

import java.util.List;

//This file is a java class since Room's generated Impl class needs user parameter of insertUser to be final.
@Dao
public abstract class UserDao {
    @Query("SELECT * FROM user")
    public abstract Single<List<User>> getAll();

    @Query("SELECT * FROM user LIMIT 1")
    public abstract Single<User> getOneUser();

    @Query("SELECT * FROM user WHERE email LIKE :email LIMIT 1")
    public abstract Single<User> getUserByEmail(String email);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public abstract Completable insertUser(final User user);

    @Delete
    public abstract void deleteUserNonCompletable(User user);

    @Query("DELETE FROM user WHERE email LIKE :email")
    public abstract void deleteByEmailNonCompletable(String email);

    @Query("DELETE FROM user")
    public abstract void deleteAllUsersNonCompletable();

    //Room's Completable and Single wrapper for delete query methods is broken in 2.1-alpha02, so we need to create
    //custom wrapper function.
    public Completable deleteAllUsers() {
        return Completable.fromAction(new Action() {
            @Override
            public void run() throws Exception {
                deleteAllUsersNonCompletable();
            }
        });
    }
}

