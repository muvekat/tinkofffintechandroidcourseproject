package com.example.movie.fintechapp.data.network.vo

import androidx.room.Entity
import com.google.gson.annotations.SerializedName

@Entity
data class Homeworks(
    @SerializedName("homeworks") val homeworkList: List<Homework>
)

@Entity
data class Homework(
    @SerializedName("title") val title:String,
    @SerializedName("tasks") val taskWrapperList:List<TaskWrapper>
)

@Entity
data class TaskWrapper(
    @SerializedName("status") val status: String,
    @SerializedName("mark") val mark: String,
    @SerializedName("task") val task:Task
)

@Entity
data class Task(
    @SerializedName("title") val title:String,
    @SerializedName("task_type") val taskType:String,
    @SerializedName("short_name") val shortName:String,
    @SerializedName("contest_info") val contestInfo:ContestInfo?
){
    val contestStatus: SerializedStatus?
        get() = contestInfo?.contestStatus?.status

    val contestUrl: String?
        get() = contestInfo?.contestUrl
}

@Entity
data class ContestInfo(
    @SerializedName("contest_url") val contestUrl:String,
    @SerializedName("contest_status") val contestStatus:ContestStatus
)

@Entity
data class ContestStatus(
    @SerializedName("status") val status:SerializedStatus
)

enum class SerializedStatus{
    @SerializedName("announcement") ANNOUNCEMENT,
    @SerializedName("ongoing") ONGOING,
    @SerializedName("contest_review") REVIEW,
    @SerializedName("accepted") ACCEPTED
}

data class TaskTransformed(
    val title: String,
    val taskType: String,
    val shortName: String,
    val contestStatus: SerializedStatus?,
    val contestUrl: String?,
    val taskStatus: String?,
    val mark: String?
)