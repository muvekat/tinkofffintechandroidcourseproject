package com.example.movie.fintechapp.ui.login

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.example.movie.fintechapp.R
import com.example.movie.fintechapp.ui.home.HomeActivity
import com.example.movie.fintechapp.ui.State
import com.example.movie.fintechapp.utils.showSnackbar
import com.example.movie.fintechapp.utils.startSingleActivityFromClass
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject

class LoginActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var mLoginViewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        initializeActivity()
    }

    override fun onStop() {
        super.onStop()
        mLoginViewModel.dispose()
    }

    private fun initializeActivity() {
        loginButton.setOnClickListener {
            login()
        }

        mLoginViewModel.loginState.observe(this, Observer { state ->
            when (state!!) {
                State.SUCCESS -> startHomeActivity()
                State.ERROR -> showError()
                State.LOADING -> showLoading()
            }
        })
    }

    private fun login() {
        val login = loginEditText.text.toString()
        val password = passwordEditText.text.toString()

        if (!login.isEmpty() && !password.isEmpty()) {
            mLoginViewModel.logIn(login, password)
        }
    }

    private fun showLoading() {
        loginEditText.isEnabled = false
        passwordEditText.isEnabled = false
        loginButton.isEnabled = false
        loginButton.text = ""
        loginProgressBar.visibility = View.VISIBLE
    }

    private fun hideLoading() {
        loginEditText.isEnabled = true
        passwordEditText.isEnabled = true
        loginButton.isEnabled = true
        loginButton.text = getText(R.string.login_button)
        loginProgressBar.visibility = View.GONE
    }

    private fun showError(){
        hideLoading()
        showSnackbar(loginLayout, R.string.error_occured)
    }

    private fun startHomeActivity(){
        hideLoading()
        this.startSingleActivityFromClass(HomeActivity::class.java)
    }

}
