package com.example.movie.fintechapp.data.network.api

import com.example.movie.fintechapp.data.network.vo.Homeworks
import com.example.movie.fintechapp.data.network.vo.ProblemWrapped
import com.example.movie.fintechapp.data.network.vo.Status
import com.example.movie.fintechapp.data.network.vo.TestAnswers
import com.example.movie.fintechapp.data.sharedents.User
import com.google.gson.JsonObject
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.*

interface TinkoffService {

    @FormUrlEncoded
    @POST("api/signin")
    fun postSignIn(@Field("email") email: String, @Field("password") password: String): Single<User>

    @GET("api/course/android_fall2018/homeworks")
    fun getHomewokrs(): Single<Homeworks>

    @GET("api/user")
    fun getUserInfo(): Single<JsonObject>

    @PUT("api/register_user")
    fun postUserInfo(@Body body: JsonObject): Completable

    @POST("api/contest/{contest_url}/start_contest")
    fun postStartContest(@Path("contest_url") contestUrl: String, @Body obj: Any = Any()): Completable

    @GET("api/contest/{contest_url}/status")
    fun getContestStatus(@Path("contest_url") contestUrl: String): Single<Status>

    @GET("api/contest/{contest_url}/problems")
    fun getContestProblems(@Path("contest_url") contestUrl: String): Single<List<ProblemWrapped>>

    @POST("api/contest/{contest_url}/problem/{position}")
    fun postContest(
        @Path("contest_url") contestUrl: String, @Path("position") position: Int,
        @Body testAnswers: TestAnswers
    ): Completable

    @POST("api/signout")
    fun postSignOut(): Completable

}