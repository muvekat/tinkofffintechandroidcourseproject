package com.example.movie.fintechapp.ui.test

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import com.example.movie.fintechapp.data.DataManager
import com.example.movie.fintechapp.data.network.vo.ContestTransformed
import com.example.movie.fintechapp.data.network.vo.ProblemTransformed
import com.example.movie.fintechapp.ui.State
import com.example.movie.fintechapp.utils.SingleLiveEvent
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class TestViewModel @Inject constructor(private val mDataManager: DataManager) {

    val stateLiveData = MutableLiveData<State>()
    val contestLiveData = MutableLiveData<ContestTransformed>()
    val problemListLiveData = MutableLiveData<List<ProblemTransformed>>()
    val openNextLiveEvent = SingleLiveEvent<Unit>()

    private val mCompositeDisposable = CompositeDisposable()

    init {
        stateLiveData.postValue(State.LOADING)
        mCompositeDisposable.add(
            mDataManager.getContestInfo()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ contest ->
                    stateLiveData.value = State.SUCCESS
                    contestLiveData.value = contest
                    problemListLiveData.value = contest.problemList
                }, {
                    stateLiveData.value = State.ERROR
                })
        )
    }

    fun submitAnswersAndOpenNext(answer: String, index: Int, problemPosition: Int) {
        stateLiveData.value = State.LOADING
        mCompositeDisposable.add(
            mDataManager.submitAnswer(answer, problemPosition)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    stateLiveData.value = State.SUCCESS

                    problemListLiveData.value?.get(index)?.lastSubmission = answer
                    problemListLiveData.value?.get(index)?.status = "AC"
                    //Trigger problemListLiveData change.
                    problemListLiveData.value = problemListLiveData.value

                    openNextLiveEvent.call()
                }, {
                    stateLiveData.value = State.ERROR
                })
        )
    }

    fun openNext() {
        openNextLiveEvent.call()
    }

    fun dispose(){
        mCompositeDisposable.clear()
    }

}