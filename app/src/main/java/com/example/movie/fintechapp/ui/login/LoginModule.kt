package com.example.movie.fintechapp.ui.login

import com.example.movie.fintechapp.dagger.scopes.ActivityScope
import com.example.movie.fintechapp.data.DataManager
import dagger.Module
import dagger.Provides

@Module
class LoginModule{
    @Provides
    @ActivityScope
    fun provideLoginViewModel(dataManager: DataManager) = LoginViewModel(dataManager)
}