package com.example.movie.fintechapp.data

import com.example.movie.fintechapp.data.local.LocalDatabaseModule
import com.example.movie.fintechapp.data.local.db.UserDao
import com.example.movie.fintechapp.data.network.NetworkServiceModule
import com.example.movie.fintechapp.data.network.api.TinkoffService
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(includes = [LocalDatabaseModule::class, NetworkServiceModule::class])
class DataModule {
    @Provides
    @Singleton
    fun provideDataRepository(userDao: UserDao, tinkoffService: TinkoffService, gson: Gson) =
        DataRepository(userDao, tinkoffService, gson)

    @Provides
    @Singleton
    fun provideDataManager(repo: DataRepository) = DataManager(repo)
}