package com.example.movie.fintechapp.data.network.vo

import androidx.room.Entity
import com.google.gson.annotations.SerializedName

@Entity
data class ProblemWrapped(
    @SerializedName("position") val position:Int,
    @SerializedName("status") val status:String?,
    @SerializedName("last_submission") val lastSubmission:LastSubmission?,
    @SerializedName("problem") val problem:Problem
)

@Entity
data class LastSubmission(
    @SerializedName("file") val answer:String
)

@Entity
data class Problem(
    @SerializedName("problem_type") val problemType:String,
    @SerializedName("cms_page") val cmsPage:CmsPage,
    @SerializedName("answer_choices") val answerChoices:List<String>
)

@Entity
data class CmsPage(
    @SerializedName("unstyled_statement") val unstyledStatement:String
)

class ProblemTransformed(
    val position: Int,
    var status: String?,
    var lastSubmission: String?,
    val problemType: String,
    val answerChoices: List<String>,
    val unstyledStatement: String
)