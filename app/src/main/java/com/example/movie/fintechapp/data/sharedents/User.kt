package com.example.movie.fintechapp.data.sharedents

import androidx.room.Entity

@Entity(primaryKeys = ["email"], tableName = "user")
data class User(
    var birthday: String?,
    val email: String,
    var firstName: String?,
    var middleName: String?,
    var lastName: String?,
    var skypeLogin: String?,
    var description: String?,
    var phoneMobile: String?,
    var region: String?,
    var school: String?,
    var schoolGraduation: Int?,
    var university: String?,
    var universityGraduation: Int?,
    var avatar: String?,
    var currentWork: String?,
    var department: String?,
    var faculty: String?,
    var grade: String?,
    var resume: String?
) {
    constructor() : this(
        null,
        "",
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null
    )
}