package com.example.movie.fintechapp.ui.home.profile

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR
import com.example.movie.fintechapp.data.sharedents.User

class ObservableUser constructor(user: User) : BaseObservable() {
    @get:Bindable
    var birthday: String? = user.birthday
        set(value) {
            if (field != value) {
                field = value
                notifyPropertyChanged(BR.birthday)
            }
        }

    @get:Bindable
    val email: String = user.email

    @get:Bindable
    var firstName: String? = user.firstName
        set(value) {
            if (field != value) {
                field = value
                notifyPropertyChanged(BR.firstName)
            }
        }

    @get:Bindable
    var middleName: String? = user.middleName
        set(value) {
            if (field != value) {
                field = value
                notifyPropertyChanged(BR.middleName)
            }
        }

    @get:Bindable
    var lastName: String? = user.lastName
        set(value) {
            if (field != value) {
                field = value
                notifyPropertyChanged(BR.lastName)
            }
        }

    @get:Bindable
    var skypeLogin: String? = user.skypeLogin
        set(value) {
            if (field != value) {
                field = value
                notifyPropertyChanged(BR.skypeLogin)
            }
        }

    @get:Bindable
    var description: String? = user.description
        set(value) {
            if (field != value) {
                field = value
                notifyPropertyChanged(BR.description)
            }
        }

    @get:Bindable
    var phoneNumber: String? = user.phoneMobile
        @Bindable
        set(value) {
            if (field != value) {
                field = value
                notifyPropertyChanged(BR.phoneNumber)
            }
        }

    @get:Bindable
    var region: String? = user.region
        set(value) {
            if (field != value) {
                field = value
                notifyPropertyChanged(BR.region)
            }
        }

    @get:Bindable
    var school: String? = user.school
        set(value) {
            if (field != value) {
                field = value
                notifyPropertyChanged(BR.school)
            }
        }


    @get:Bindable
    var schoolGraduation: Int? = user.schoolGraduation
        set(value) {
            if (field != value) {
                field = value
                notifyPropertyChanged(BR.schoolGraduation)
            }
        }

    @get:Bindable
    var university: String? = user.university
        set(value) {
            if (field != value) {
                field = value
                notifyPropertyChanged(BR.university)
            }
        }

    @get:Bindable
    var universityGraduation: Int? = user.universityGraduation
        set(value) {
            if (field != value) {
                field = value
                notifyPropertyChanged(BR.universityGraduation)
            }
        }

    @get:Bindable
    var avatar: String? = user.avatar
        set(value) {
            if (field != value) {
                field = value
                notifyPropertyChanged(BR.avatar)
            }
        }

    @get:Bindable
    var currentWork: String? = user.currentWork
        set(value) {
            if (field != value) {
                field = value
                notifyPropertyChanged(BR.currentWork)
            }
        }

    @get:Bindable
    var department: String? = user.department
        set(value) {
            if (field != value) {
                field = value
                notifyPropertyChanged(BR.department)
            }
        }

    @get:Bindable
    var faculty: String? = user.faculty
        set(value) {
            if (field != value) {
                field = value
                notifyPropertyChanged(BR.faculty)
            }
        }

    @get:Bindable
    var grade: String? = user.grade
        set(value) {
            if (field != value) {
                field = value
                notifyPropertyChanged(BR.grade)
            }
        }

    @get:Bindable
    var resume: String? = user.resume
        set(value) {
            if (field != value) {
                field = value
                notifyPropertyChanged(BR.resume)
            }
        }

    fun getResultingUser(): User {
        return User(
            birthday,
            email,
            firstName,
            middleName,
            lastName,
            skypeLogin,
            description,
            phoneNumber,
            region,
            school,
            schoolGraduation,
            university,
            universityGraduation,
            avatar,
            currentWork,
            department,
            faculty,
            grade,
            resume
        )
    }
}