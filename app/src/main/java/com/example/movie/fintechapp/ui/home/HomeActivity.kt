package com.example.movie.fintechapp.ui.home

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.lifecycle.Observer
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.example.movie.fintechapp.R
import com.example.movie.fintechapp.data.DataManager
import com.example.movie.fintechapp.ui.home.course.CourseFragment
import com.example.movie.fintechapp.ui.home.course.CourseViewModel
import com.example.movie.fintechapp.ui.home.profile.ProfileFragment
import com.example.movie.fintechapp.ui.home.profile.ProfileViewModel
import com.example.movie.fintechapp.ui.home.settings.SettingsFragment
import com.example.movie.fintechapp.ui.home.settings.SettingsViewModel
import com.example.movie.fintechapp.ui.login.LoginActivity
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_home.*
import java.lang.Error
import javax.inject.Inject



class HomeActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var mDataManager: DataManager

    @Inject
    lateinit var mCourseFragment: CourseFragment

    @Inject
    lateinit var mProfileFragment: ProfileFragment

    @Inject
    lateinit var mSettingsFragment: SettingsFragment


    //Store ViewModels to call dispose() on activity's stop.
    @Inject
    lateinit var mCourseViewModel: CourseViewModel
    @Inject
    lateinit var mProfileViewModel: ProfileViewModel
    @Inject
    lateinit var mSettingsViewModel: SettingsViewModel


    private val mViewPagerAdapter = HomeFragmentPageAdapter(supportFragmentManager)

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_course -> {
                homeToolbar.title = getString(R.string.course_name)
                fragmentHolder.currentItem = 0
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_profile -> {
                homeToolbar.title = getString(R.string.profile_tab_title)
                fragmentHolder.currentItem = 1
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_settings -> {
                homeToolbar.title = getString(R.string.settings_tab_name)
                fragmentHolder.currentItem = 2
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }


    private val mIsSignedOffObserver = Observer<Boolean> {
        if (it) {
            val intent = Intent(this, LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or  Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        initializeActivity()
    }

    override fun onStop() {
        super.onStop()
        mCourseViewModel.dispose()
        mProfileViewModel.dispose()
        mSettingsViewModel.dispose()
    }

    private fun initializeActivity() {
        fragmentHolder.adapter = mViewPagerAdapter
        fragmentHolder.offscreenPageLimit = mViewPagerAdapter.count - 1

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        mDataManager.isSignedOff.observe(this, mIsSignedOffObserver)

        navigation.selectedItemId = R.id.navigation_profile
    }

    inner class HomeFragmentPageAdapter(fm: FragmentManager): FragmentPagerAdapter(fm) {
        override fun getItem(position: Int): Fragment {
            return when(position){
                0 -> mCourseFragment
                1 -> mProfileFragment
                2 -> mSettingsFragment
                else -> throw Error("HomeFragmentPageAdapter got non-expected position value!")
            }
        }

        override fun getCount(): Int {
            return 3
        }
    }

}
