package com.example.movie.fintechapp.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.movie.fintechapp.data.network.vo.*
import com.example.movie.fintechapp.data.sharedents.User
import com.example.movie.fintechapp.utils.isForbidden
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import java.lang.Exception
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DataManager @Inject constructor(
    private val mDataRepository: DataRepository
) {
    //Using LiveData to avoid using synchronized getters and setters.
    private val mCachedUser = MutableLiveData<User?>().apply { postValue(null) }
    private val mCachedTaskList = MutableLiveData<List<TaskTransformed>?>().apply { postValue(null) }
    private val mCachedLink = MutableLiveData<String?>().apply { postValue(null) }


    private val mIsSignedOff = MutableLiveData<Boolean>().apply { postValue(true) }

    /**
     * LiveData which value is True if user is signed off (so that we can show login activity).
     */
    val isSignedOff: LiveData<Boolean>
        get() = mIsSignedOff

    /**
     * Sign in with given credentials.
     *
     * @return Completable which completes if sign in was successful or gives error otherwise.
     */
    fun signIn(email: String, password: String): Completable {
        return mDataRepository.signIn(email, password)
            .flatMapCompletable {
                cacheUserAndPostFalseToIsSignedOff(it)
                Completable.complete()
            }
    }

    /**
     * Get user from cached value and in case it's empty try getting it from network or local storage if phone is offline.
     */
    fun getUser(): Single<User> {
        val cachedUser = mCachedUser.value
        if (cachedUser != null)
            return Single.just(cachedUser)

        return getUserRemoteInternal()
            .onErrorResumeNext {
                if (!it.isForbidden()) {
                    //Assume that we are offline and return single which gets user from database.
                    mDataRepository.getUserLocal()
                } else {
                    throw it
                }
            }
            .map {
                cacheUserAndPostFalseToIsSignedOff(it)
                it
            }
    }

    /**
     * Force getting user from remote source.
     */
    fun getUserRemote(): Single<User> {
        return getUserRemoteInternal()
            .map {
                cacheUserAndPostFalseToIsSignedOff(it)
                it
            }
    }

    /**
     * Force getting cached user.
     */
    fun getUserCached(): Single<User> {
        val cachedUser = mCachedUser.value
        if (cachedUser != null)
            return Single.just(cachedUser)

        return mDataRepository.getUserLocal()
            .map {
                cacheUserAndPostFalseToIsSignedOff(it)
                it
            }
    }

    private fun cacheUserAndPostFalseToIsSignedOff(it: User) {
        mCachedUser.postValue(it)
        mIsSignedOff.postValue(false)
    }

    private fun getUserRemoteInternal(): Single<User> {
        return mDataRepository.getUserRemote()
            .doOnError { error ->
                if (error.isForbidden()) {
                    deleteCachedUserAndPostTrueToIsSignedOff()
                }
            }
    }

    /**
     * Get list of tasks sorted by status.
     */
    fun getTaskList(): Single<List<TaskTransformed>> {
        val taskList = mCachedTaskList.value
        if (taskList != null)
            return Single.just(taskList)

        return getTaskListRemote()
    }

    fun getTaskListRemote(): Single<List<TaskTransformed>> {
        return mDataRepository.getHomeworks()
            .doOnError { error ->
                if (error.isForbidden()) {
                    deleteCachedUserAndPostTrueToIsSignedOff()
                }
            }
            //Map can be computationally intensive task, so we might want to move computations from (limited) io pool.
            .observeOn(Schedulers.computation())
            .map { homeworkList ->
                val resultTaskList = mutableListOf<TaskTransformed>()
                homeworkList.forEach { homework ->
                    val internalTaskList = homework.taskWrapperList.map { wrapper ->
                        TaskTransformed(
                            wrapper.task.title,
                            wrapper.task.taskType,
                            wrapper.task.shortName,
                            wrapper.task.contestStatus,
                            wrapper.task.contestUrl,
                            wrapper.status,
                            wrapper.mark
                        )
                    }
                    resultTaskList.addAll(internalTaskList.filter { it.taskType == "test_during_lecture" })
                }
                resultTaskList.sortBy { task -> task.contestStatus }

                mCachedTaskList.postValue(resultTaskList)

                resultTaskList as List<TaskTransformed>
            }
    }

    /**
     * Try starting a contest.
     */
    fun startContest(link: String): Completable {
        return mDataRepository.startContest(link)
            .andThen(Completable.fromAction { mCachedLink.postValue(link) })
    }

    fun getContestInfo(): Single<ContestTransformed> {
        val link = mCachedLink.value ?: return Single.error(Exception("Cached link is empty!"))

        return Single.zip(mDataRepository.getContestStatus(link),
            mDataRepository.getContestProblems(link),
            BiFunction<Status, List<ProblemWrapped>, ContestTransformed> { status, listOfProblemWrapped ->
                val listOfProblems = listOfProblemWrapped.map { wrapped ->
                    ProblemTransformed(
                        wrapped.position,
                        wrapped.status,
                        wrapped.lastSubmission?.answer,
                        wrapped.problem.problemType,
                        wrapped.problem.answerChoices,
                        wrapped.problem.cmsPage.unstyledStatement
                    )
                }
                ContestTransformed(
                    status.contest.title,
                    status.contest.duration,
                    status.contest.timeLeft,
                    listOfProblems
                )
            }
        )
    }

    /**
     * Submit provided answers for a current contest's problem.
     */
    fun submitAnswer(answer: String, problemPosition: Int): Completable {
        val link = mCachedLink.value ?: return Completable.error(Exception("Cached link is empty!"))

        return mDataRepository.postAnswers(link, problemPosition, answer)
    }

    /**
     * Make a request to edit current logged in user.
     */
    fun editUser(user: User): Completable {
        return mDataRepository.editUser(user)
            .doOnError { error ->
                if (error.isForbidden()) {
                    deleteCachedUserAndPostTrueToIsSignedOff()
                }
            }
            .andThen(Completable.fromAction { mCachedUser.postValue(user) })
    }

    /**
     * Make a request to sign out.
     */
    fun signOut(): Completable {
        return mDataRepository.signOut()
            .onErrorComplete { error ->
                error.isForbidden()
            }
            .andThen(Completable.fromAction { deleteCachedUserAndPostTrueToIsSignedOff() })
    }

    private fun deleteCachedUserAndPostTrueToIsSignedOff() {
        mIsSignedOff.postValue(true)
        mCachedUser.postValue(null)
    }
}

