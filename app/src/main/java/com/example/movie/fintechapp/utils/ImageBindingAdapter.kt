package com.example.movie.fintechapp.utils

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.example.movie.fintechapp.R
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions

private val options = RequestOptions()
    .placeholder(R.drawable.ic_user_placeholder)
    .error(R.drawable.ic_user_placeholder)
    .circleCrop()
    .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
    .priority(Priority.NORMAL)

@BindingAdapter("imageUrl")
fun loadImage(imageView: ImageView, url: String) {
    if (!url.isEmpty()) {
        Glide.with(imageView.context)
            .load(url)
            .apply(options)
            .into(imageView)
    } else {
        imageView.setImageResource(R.drawable.ic_user_placeholder)
    }
}