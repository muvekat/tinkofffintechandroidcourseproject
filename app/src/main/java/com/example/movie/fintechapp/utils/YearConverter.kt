package com.example.movie.fintechapp.utils

import androidx.databinding.InverseMethod

object YearConverter{
    @JvmStatic
    @InverseMethod("stringToInt")
    fun intToString(
        value: Int?
    ): String? {
        if (value != null)
            return value.toString()
        return null
    }

    @JvmStatic
    fun stringToInt(
        value: String?
    ): Int? {
        if (value != null && !value.isEmpty())
            return value.toInt()
        return null
    }
}