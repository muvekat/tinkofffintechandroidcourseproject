package com.example.movie.fintechapp

import android.app.Application
import android.content.Context
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class TestAppModule(private var application: Application) {

    @Provides
    @Singleton
    fun provideApp(): Application = application

}