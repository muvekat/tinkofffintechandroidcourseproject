package com.example.movie.fintechapp

import android.app.Application
import androidx.test.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import com.example.movie.fintechapp.data.DataManager
import com.example.movie.fintechapp.data.DataRepository

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.junit.Before
import javax.inject.Inject

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {

    @Inject lateinit var repo:DataRepository
    @Inject lateinit var manager: DataManager

    @Before
    fun setup(){
        val app = InstrumentationRegistry.getTargetContext().applicationContext as Application
        val component = DaggerTestComponent.builder().appContext(app).build().inject(this)
    }

    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getTargetContext()
        assertEquals("com.example.movie.fintechapp", appContext.packageName)
    }

}
