package com.example.movie.fintechapp

import android.app.Application
import com.example.movie.fintechapp.data.DataModule
import com.example.movie.fintechapp.data.DataRepository
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(modules = [DataModule::class])
interface TestComponent  {

    fun inject(test: ExampleInstrumentedTest)

    @Component.Builder
    interface Builder  {
        fun build():TestComponent

        @BindsInstance
        fun appContext( context: Application): Builder

    }
}